\context ChordNames 
	\chords {
		\set chordChanges = ##t
		% intro
		e1:m e1:m

		% perdoname, sennor...
		e1:m d1 c2 a2:m b1:7
		e1:m d1 c2 a2:m b1:7

		% a ti...
		g1 d1 c2 a2:m b1:7
		g1 d1 c2 b2:7 e1:m

		% perdoname, sennor...
		e1:m d1 c2 a2:m b1:7
		e1:m d1 c2 a2:m b1:7

		% piedad.
		e1:m e1:m
	}
