\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key e \minor

		R1  |
		r2 r4 r8. b 16  |
		e' 16 e' e' fis' g' 2 r8 g'  |
		fis' 8 e' 16 d' ~ d' 2.  |
%% 5
		r8 c' c' d' e' 4 e'  |
		b 2. r8. b 16  |
		e' 16 e' e' fis' g' 2 r8 g'  |
		fis' 8 e' 16 d' ~ d' 2.  |
		r8 c' c' d' e' 4 e'  |
%% 10
		dis' 2. r8. g' 16  |
		g' 4. fis' 8 g' g' b' b' ~  |
		b' 8 fis' g' fis' 4. r8. e' 16  |
		e' 4. dis' 8 e' e' g' fis' ~  |
		fis' 2 r4 g' 8. fis' 16  |
%% 15
		g' 4. fis' 8 g' g' b' b' ~  |
		b' 8 fis' g' fis' 4. r8. e' 16  |
		e' 8 e' g' fis' ~ fis' g' fis' e' ~  |
		e' 2. r8 r16 b  |
		e' 16 e' e' fis' g' 2 r8 g'  |
%% 20
		fis' 8 e' 16 d' ~ d' 2.  |
		r8 c' c' d' e' 4 e'  |
		b 2. r8 r16 b  |
		e' 16 e' e' fis' g' 2 r8 g'  |
		fis' 8 e' 16 d' ~ d' 2.  |
%% 25
		r8 c' c' d' e' 4 e'  |
		dis' 2. dis' 4  |
		e' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Per -- dó -- na -- me, Se -- ñor,
		per -- dó -- na -- me, y ten pie -- dad de mí.
		Per -- dó -- na -- me, Se -- ñor,
		per -- dó -- na -- me, y ten pie -- dad de mí.

		A ti, te pi -- do, oh, Cris -- to, per -- dón,
		a ti, te pi -- do pie -- dad. __
		Hoy de ti quie -- ro re -- ci -- bir con a -- mor
		tu paz, tu per -- don y pie -- dad. __

		Per -- dó -- na -- me, Se -- ñor,
		per -- dó -- na -- me, y ten pie -- dad de mí.
		Per -- dó -- na -- me, Se -- ñor,
		per -- dó -- na -- me, y ten pie -- dad de mí,
		pie -- dad.
	}
>>
